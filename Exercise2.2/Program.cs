﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    //Contraint Class chir cho phep bien tham chieu(Reference value)
    public class CollectionGeneric<T> where T : class
    {
        private T[] arr;
        //Khoi tao 

        //public T[] getTypeGeneric()
        //{
        //    return arr;
        //}
        //Khởi tạo 1 collection
        public CollectionGeneric(int collectionLength)
        {
            arr = new T[collectionLength];
        }
        public void Add_Ger(params T[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                arr[i] = list[i];
            }
        }
        public void ShowTypeAndValue(T[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                Console.WriteLine( "Gia tri: " + list[i] + "-- Kieu: " + list.GetType()); 
            }


        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            string[] arr = { "ab", "cd", "asd"};
            var collection = new CollectionGeneric<string>(10);
            collection.Add_Ger(arr);
            //In ra gia tri va thuoc tinh cua phan tu trong mang
            collection.ShowTypeAndValue(arr);
            Console.ReadKey();
        }
    }
}
