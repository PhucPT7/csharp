﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter content:");
            string content = Console.ReadLine();
            Console.WriteLine("Enter maxLenght:");
            int maxlenght = Int32.Parse(Console.ReadLine());

            string result = GetArticleSummary1(content, maxlenght);
            Console.WriteLine("Summary:{0}", result);

            Console.ReadKey();
        }
        static public string GetArticleSummary1(string content, int maxLength)
        {
            string result;
            //check length of content
            if (content.Length > maxLength)
            {
                result = content.Substring(0, maxLength) + "...";
            }
            else result = content;

            return result;
        }
    }
}
