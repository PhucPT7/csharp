﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Array of integer:");
            string arr = Console.ReadLine();
            Console.WriteLine("Enter subLength:");
            int subLength = Int32.Parse(Console.ReadLine());

            //Căt chuỗi bởi dấu phẩy rồi convert sang intArray
            int[] IntArray = arr.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            int result = FindMaxSubArray(IntArray, subLength);

            Console.WriteLine("ressult:{0}", result);
            Console.ReadKey();
        }
        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            int result = 0;
            for (int i = 0; i < subLength; i++)
            {
                result += inputArray[i];
            }
            return result;

        }
    }
}
