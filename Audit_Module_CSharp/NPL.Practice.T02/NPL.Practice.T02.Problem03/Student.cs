﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{

    public class Student : IGraduate
    {
        int _Id { get; set; }
        string _Name;
        public DateTime _StartDate { get; set; }
        public decimal _SqlMark { get; set; }
        public decimal _CsharpMark { get; set; }
        public decimal _DsaMark { get; set; }
        public decimal _GPA { get; set; }
        public GraduateLevel _GraduateLevel;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public decimal DsaMark
        {
            get { return _DsaMark; }
            set { _DsaMark = value; }
        }


        public decimal CsharpMark
        {
            get { return _CsharpMark; }
            set { _CsharpMark = value; }
        }
        public decimal SqlMark
        {
            get { return _SqlMark; }
            set { _SqlMark = value; }
        }


        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public Student() { }
        public Student(int id, string name, DateTime startdate, decimal sqlmark, decimal csmark, decimal dsamark)
        {
            _Id = id;
            _Name = name;
            _StartDate = startdate;
            _SqlMark = sqlmark;
            _CsharpMark = csmark;
            _DsaMark = dsamark;
        }
        public string GetCertificate()
        {

            string str = " Name:" + Name + " StartDate:" + StartDate +
                " Sql:" + SqlMark + " Csharp:" + CsharpMark + " Dsa:" + DsaMark + " Gpa:" + _GPA + " Rank: " + _GraduateLevel;
            return str;
        }
        public void Graduate()
        {
            var gpa = (_SqlMark + _CsharpMark + _DsaMark) / 3;
            _GPA = Math.Round(gpa, 2);
            if (_GPA >= 9)
            {
                _GraduateLevel = GraduateLevel.Excellent;
            }
            else if (_GPA >= 8 && _GPA < 9)
            {
                _GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (_GPA >= 7 && _GPA < 8)
            {
                _GraduateLevel = GraduateLevel.Good;
            }
            else if (_GPA >= 5 && _GPA < 7)
            {
                _GraduateLevel = GraduateLevel.Average;
            }
            else
            {
                _GraduateLevel = GraduateLevel.Failed;
            }


        }
    }

}
