﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    class Program
    {       
        static void Main(string[] args)
        {

            //tạo student và gán dữ liệu mẫu
            Student student = new Student();
            student.Id = 1;
            student.Name = "Thanh Phuc";
            student.StartDate = DateTime.Now;
            student.SqlMark = 5;
            student.CsharpMark = 9.5m;
            student.DsaMark = 5;

            //chạy method Graduate()
            student.Graduate();

            //chạy method GetCertificate()
            var certificate = student.GetCertificate();
            Console.WriteLine(certificate);
            Console.ReadKey();
        }
    }
}
