﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{

    public enum GraduateLevel
    {
        Excellent,
        VeryGood,
        Good,
        Average,
        Failed

    }
}
