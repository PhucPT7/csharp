﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List_Collection
{
    public class Movie 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Diretor { get; set; }

    }
   
    public class PlayMovie
    {
        public void ShowMovie(List<Movie> m)
        {
            Console.WriteLine("\n\n ShowMovie:");
            foreach (var item in m)
            {
                Console.WriteLine("\nId:" + item.ID + " Name:" + item.Name + " Director:" + item.Diretor);
            } 
                       
        }
        public void SortMovie(List<Movie> m)
        {

            List<Movie> result= m.OrderByDescending(x => x.ID).ToList();
            Console.WriteLine("\n\n Sort Movie:");
            foreach(var item in result)
            {
                Console.WriteLine("\nId:" + item.ID + " Name:" + item.Name + " Director:" + item.Diretor);
            }    
        }
        public void FindMovieByName(List<Movie> listmovie, string name)
        {

            var result = listmovie.Find(x => x.Name== name);
            Console.WriteLine("\n\n FindByName:");
            Console.WriteLine("\nId:" + result.ID + " Name:" + result.Name + " Director:" + result.Diretor);
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Movie> movie = new List<Movie> {
            new Movie {ID=1, Name = "Movie1", Diretor = 1 },
            new Movie {ID=2, Name = "Movie2", Diretor = 2 },
            new Movie {ID=4, Name = "Movie3", Diretor = 3 },
            new Movie {ID=3, Name = "Movie4", Diretor = 4 }
    };
            var movie2 = new PlayMovie();            
            //movie.Remove(movie[1]); //Remove or RemoveAt
            //movie.RemoveAt(2);
            //Them gia tri tai vi tri index 2
            movie.Insert(2, new Movie { ID = 20, Name = "Phuc", Diretor = 2 });
            //Show danh sach movie
            movie2.ShowMovie(movie);
            //sap xep giam dan theo Id
            movie2.SortMovie(movie);
            //find by name
            movie2.FindMovieByName(movie, "Movie2");

            Console.ReadKey();

        }
    }
}

