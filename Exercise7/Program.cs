﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Demo
{
    
    public class Student
    {
        int studentId;
        string name;
        DateTime birth;
        int gender;
        public int StudentID
        {
            get { return studentId; }
            set { studentId = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }        
        public DateTime Birth
        {
            get { return birth; }
            set { birth = value; }
        }
        public int Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        
    }

    delegate Student DelegateStudent(ref Student dStudent);

    class Program
    {
        static Student student = new Student();
        static Student PrintStudent(ref Student student)
        {
            Console.Write("Result:" + student.StudentID + " Name: " + student.Name + " Birth:" + student.Birth + " gender:" + student.Gender);
            return student;

        }
        static Student InputStudent(ref Student student)
        {
            Console.Write("Nhap Id:");
            student.StudentID = Int32.Parse(Console.ReadLine());
            Console.Write("Nhap Name:");
            student.Name = Console.ReadLine();
            Console.Write("Nhap Ngay sinh:");
            student.Birth = DateTime.Parse(Console.ReadLine());
            Console.Write("Nhap gender:");
            student.Gender = Int32.Parse(Console.ReadLine());
            return student;
        }

        static void Main(string[] args)
        {
            var student = new Student();
            DelegateStudent delegate1 = new DelegateStudent(InputStudent);
            DelegateStudent delegate2 = new DelegateStudent(PrintStudent);
            delegate1(ref student);
            delegate2(ref student);

            Console.ReadKey();

            
        }
    }
}
