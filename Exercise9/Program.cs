﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Shape
    {
        public int Width;
        public int Height;
        public virtual int Area(int width, int height)
        {
            return width * height;
        }

    }
    class Rectangle:Shape
    {
        public override int Area(int width, int height)
        {
            return width * height;
        }

        public void Size(int sameSize)
        {
            Width = Height = sameSize;
        }

        public void Size(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public Rectangle()
        {

        }
        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public static Rectangle operator +(Rectangle a, Rectangle b)
            => new Rectangle(a.Width + b.Width, a.Height + b.Height);

        public static Rectangle operator -(Rectangle a, Rectangle b)
            => new Rectangle(a.Width - b.Width, a.Height - b.Height);

    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rec1 = new Rectangle();
            int a = 3, b = 2;
            var result = rec1.Area(a, b);
            Console.WriteLine("Dien tich cua {0} va {1} la: {2}",a,b ,result);
            int c = 10, d = 8;
            Rectangle rec2 = new Rectangle(c,d);//(10,8)
            Rectangle rec3 = new Rectangle(a,b);//(3,2)
            var Sumrec = rec2 + rec3;
            Console.WriteLine("Sum:(" + Sumrec.Width +","+ Sumrec.Height + ")");
            var exceptrec = rec2 - rec3;
            Console.WriteLine("Except:("+ exceptrec.Width +","+ exceptrec.Height+")");

            Console.ReadKey();

        }
    }
}
