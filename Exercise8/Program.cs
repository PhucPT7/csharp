﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Students
    {
        string name;
        int age;
        int _class;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public int IdClass
        {
            get { return _class; }
            set { _class = value; }
        }
        public Students(string n, int a, int _Class)
        {
            name = n;
            age = a;
            _class = _Class;
        }

    }
    class ClassList
    {
        int IdClass;
        string nameclass;
        public int IDClass
        {
            get { return IdClass; }
            set { IdClass = value; }
        }
        public string NameClass
        {
            get { return nameclass; }
            set { nameclass = value; }
        }
        public ClassList(int ic, string nc)
        {
            IdClass = ic;
            nameclass = nc;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //add student
            var student = new List<Students>();
            Students st1 = new Students("Thanh Phuc", 11, 1);
            Students st2 = new Students("Thanh Phuc2", 11, 1);
            Students st3 = new Students("Thanh Phuc3", 11, 1);
            Students st4 = new Students("Thanh Phuc4", 11, 2);
            Students st5 = new Students("Thanh Phuc5", 11, 1);
            student.Add(st1);
            student.Add(st2);
            student.Add(st3);
            student.Add(st4);
            student.Add(st5);

            //add class
            var Listclass = new List<ClassList>();
            ClassList c1 = new ClassList(1,"NET01");
            ClassList c2 = new ClassList(2,"NET02");
            Listclass.Add(c1);
            Listclass.Add(c2);

            Console.WriteLine("GroupJoin:");
            var result = Listclass.GroupJoin(student,
                                 lc => lc.IDClass,
                                st => st.IdClass,
                                
                                (lc, st12) => new
                                {
                                    _ListStudent = st12,
                                    ClassID = lc.IDClass,
                                    ClassName = lc.NameClass,
                                });
            foreach(var item in result)
            {
                Console.Write("\nClassID:" + item.ClassID + " ClassName:" + item.ClassName);
                foreach (var item1 in item._ListStudent)
                    Console.Write("\nNameStudent:" + item1.Name);
            }

            Console.WriteLine("\nSo luong student of class");
            foreach (var item in result)
            {
                int count = 0;
                foreach (var item1 in item._ListStudent)
                        count++;
                        Console.WriteLine("Class:"+item.ClassID +"Soluong:"+count);
            }
            Console.WriteLine("\nList student by class");
            foreach (var item in result)
            {
                Console.Write("\nClassID:" + item.ClassID + " ClassName:" + item.ClassName);
                string str = "";
                foreach (var item1 in item._ListStudent)
                    str = str + item1.Name +"--";              
                    Console.Write(str);
            }

            Console.ReadKey();

        }
    }
}
