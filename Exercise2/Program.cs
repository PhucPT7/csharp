﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    public class Generic_examp<T>
    {

        private T[] collections;
        private int _size = 0;
        //Khởi tạo 1 collection
        public Generic_examp(int collectionLength)
        {
            collections = new T[collectionLength];
        }
        public void Add_Ger(params T[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                collections[i] = list[i];
            }
        }

        
        //Remove
        //public T Removee(int i)
        //{
             
        //}

        public T[] ShowResult()
        {
            return collections;
        }    
    } 
    class Program
    {
        static void Main(string[] args)
        {
            //Khởi tạo mảng
            string[] arr= { "1", "3", "5", "4", "7", "8" };
            //Đếm mảng
            int count_string = arr.Count();
            //Khởi tạo 1 generic
            Generic_examp<string> ger = new Generic_examp<string>(count_string);
            ger.Add_Ger(arr);
            string[] result = ger.ShowResult();
            for (int i= 0; i < result.Length;i++)
            {
                Console.WriteLine("Item[{0}]: {1}", i, result[i]);
            }


            Console.ReadKey();

        }
    }
}
