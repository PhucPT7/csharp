﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_B
{
    class GerExerciseB<T>
    {
        // Khai báo mảng với kich thuoc
        private T[] arr = new T[100];
        public T this[int i]
        {
            get { return arr[i]; }
            set { arr[i] = value; }
        }
        public int CountGer()
        {
            int i=0;
            foreach (var item in arr)
            {
                if (item != null)
                {
                    i++;
                }
            }
            return i;
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            var result = new GerExerciseB<string>();
            result[0] = "Thanh Phuc";
            result[1] = "Thanh Phuc1";
            result[2] = "Thanh Phuc2";
            Console.WriteLine(result[0]);
            int demlist = result.CountGer();
            Console.WriteLine("So phan tu cua mang: {0}", demlist);

            Console.ReadKey();


        }
    }
}
