﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    public abstract class PaymentInfomation
    {
        private int _PaymentNo = 101;
        public virtual int GetPaymentNo()
        {
            return _PaymentNo;
        }
    }
    public interface IOrder
        {        
        void PlaceOrder();
        void CancleOrder();
        void GetOrder();

    }
    public class PizzaOrder: PaymentInfomation, IOrder
    {
        public int _price = 5000;
        public int _amount;
        public bool _isOrdered = false;
        

        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
           
        public PizzaOrder(int amount)
        {
            _amount = amount;
        }
        public void PlaceOrder()
        {
            if (_amount == 0)
            {
                Console.WriteLine("Vui long nhap amount!");
            }
            else { 
                if(_isOrdered==true)
                {
                    Console.WriteLine("ban da dat roi!");
                }    
                else
                { 
                    int total = _amount * _price;
                    int _paymentno = GetPaymentNo();
                    _isOrdered = true;
                    Console.WriteLine("PaymentNo: " + _paymentno + " Total: " + total + " IsOrder: " + _isOrdered);
                }
            }
        }
        public void CancleOrder()
        {
            if(_isOrdered==true)
            {
                _isOrdered = false;
                Console.WriteLine("Huy thanh cong !{0}", _isOrdered);
            }  
            else
            {
                Console.WriteLine("Chua dat hang!{0}", _isOrdered);
            }    
        }
        public void GetOrder()
        {
            if (_isOrdered == true)
            {
                int _paymentno = GetPaymentNo();
                var total = _amount * _price;
                Console.WriteLine("PaymentNo: " + _paymentno + " Amount: " + _amount + " Price:" + _price + " total:" + total);
            }
            else
            {
                Console.WriteLine("Chua co don hang nao!");
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var pizza = new PizzaOrder(0);
            pizza.PlaceOrder();
            pizza.Amount = 5;
            pizza.PlaceOrder();
            pizza.CancleOrder();
            pizza.PlaceOrder();
            pizza.GetOrder();


            Console.ReadKey();

        }
    }
}
