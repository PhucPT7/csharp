﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_Day4
{
    class Employee
    {
        static int Count_Check = 0;
        public int ID { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }

        public string Role { get; set; }
       
        //cap nhat Salary of employee
        public void SetSalary()
        {
            if(Role!="Admin")
            {
                Count_Check = Count_Check + 40;
                Check_vali(Count_Check);
                Console.WriteLine("Bạn không có quyền sửa đổi lương!");                
            }    
            else
            {
                Console.WriteLine("Bạn có thể sửa đổi!");
            }   
        }
        //lay info employee
        public string GetInfor()
        {
             Count_Check++;
            Check_vali(Count_Check);
            return "Id: " + ID + "\nName: " + Name + "\nSalary: " + Salary + "\nRole: " + Role + "\nCount_Check: " + Count_Check;            
        }

        //Kiểm tra giá trị Count_Check
        public void Check_vali(int a)
        {
            if (a > 40)
            {
                Console.WriteLine("\nThành phần khả nghi!(Count_Check={0})", a);
            }
            else
            {
                Console.WriteLine("\nTruy cập hợp lệ!(Count_Check={0})",a);
            }    
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Employee e = new Employee();
            e.Name = "Thanh Phuc";
            e.ID = 1;
            e.Salary = 100;
            e.Role = "NoneAdmin";
            
            e.SetSalary();

            string check = e.GetInfor();
            Console.WriteLine(check);

            Console.ReadKey();
        }
    }
}
