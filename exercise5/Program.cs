﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndDictionary
{
    class Program
    {
        class Students/*: IComparable<Students>*/
        {
            public int StudentID { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }

            public string Class { get; set; }

            public Students() { }
            public Students(int sid, string name, int age, string _class)
            {
                StudentID = sid;
                Name = name;
                Age = age;
                Class = _class;
            }
            public void CheckContainsStudent(List<Students> arr, Students s)
            {
                //
                Console.WriteLine("--Cau 3:");
                if (arr.Contains(s))
                {
                    Console.WriteLine("Student co ton tai trong mang!");

                }
                else
                {
                    Console.WriteLine("Student khong co ton tai trong mang!");
                }
            }
            public void ShowStudent(List<Students> arr)
            {
                Console.WriteLine("\n\nList Student:");
                foreach(var item in arr)
                {
                    Console.WriteLine("Id:" + item.StudentID + " Name:" +item.Name+" Age:" + item.Age + " Class"+Class);
                }    
            }
            public void CountSutdent(List<Students> arr)
            {
                int i = arr.Count();
                Console.WriteLine("So luong student: {0}",i);

            }

        }
        class StudentsComparer : IComparer<Students>
        {
            public int Compare(Students x, Students y)
            {
                return x.StudentID.CompareTo(y.StudentID);
            }
        }
        static void Main(string[] args)
        {
            List<Students> studentArr = new List<Students>();
            Students s1 = new Students(101, "Thanh Phuc", 20, "Net05");
            Students s2 = new Students(102, "Thanh Phuc2", 20, "Net04");
            Students s3 = new Students(104, "Thanh Phuc3", 20, "Net03");
            Students s4 = new Students(103, "Thanh Phuc4", 20, "Net02");
            Students s5 = new Students(105, "Thanh Phuc5", 20, "Net01");
            Students s6 = new Students(106, "Thanh Phuc6", 20, "Net00");

            studentArr.Add(s1);
            studentArr.Add(s2);
            studentArr.Add(s3);
            studentArr.Add(s4);
            studentArr.Add(s5);
            var Mstudent = new Students();
            //check contains
            Mstudent.CheckContainsStudent(studentArr, s6);

            Console.WriteLine("\nList chua sort");
            Mstudent.ShowStudent(studentArr);
            //Sort by comparer ASC(Tang dan)
            studentArr.Sort(new StudentsComparer());
            //Dao nguoc chuoi
            studentArr.Reverse();
            Console.WriteLine("\nList da sort");
            Mstudent.ShowStudent(studentArr);
            //Remove element of list
            //studentArr.Remove(s2);
            //cau 6
            Mstudent.CountSutdent(studentArr);






            Console.ReadKey();


        }
    }
}
